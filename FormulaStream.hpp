#pragma once

#include<string>
#include<optional>
#include<functional>

namespace fcalc
{
	using Operation = std::function<std::optional<double>(double, double)>;
	enum OperationPriority : int
	{
		SumPriority = 0,
		MultPriority
	};

	struct OperationInfo
	{
		Operation operation;
		OperationPriority priority;
		size_t pos;
	};

	struct IFormulaStream
	{
		virtual ~IFormulaStream() {}
		virtual const std::string& getFormula() const = 0;
		virtual size_t getPosition() const = 0;

		virtual bool isEnd() = 0;
		virtual std::optional<double> readNumber() = 0;
		virtual std::optional<OperationInfo> readOperation() = 0;
		virtual bool readOpeningBracket() = 0;
		virtual bool readClosingBracket() = 0;
	};
}