#include "stdafx.h"
#include "FormulaStreamImpl.hpp"
#include "CalculatorException.hpp"

using namespace fcalc;

enum Character : char {
	LEFT_BRACKET = '(',
	RIGHT_BRACKET = ')',
	PLUS = '+',
	MINUS = '-',
	MULT = '*',
	DIVIDE = '/',
	SPACE = ' ',
	DIGIT_0 = '0',
	DIGIT_9 = '9'
};

/////////////////////////////////////////////////////////////////////////////
FormulaStream::FormulaStream(const std::string& formula):
m_formula(formula), m_position(0)
{

}
/////////////////////////////////////////////////////////////////////////////
bool FormulaStream::isEnd() 
{
	return m_position >= m_formula.length();
}
/////////////////////////////////////////////////////////////////////////////
std::optional<double> FormulaStream::readNumber() 
{
	skipSpace();
	if (isEnd())
		return std::nullopt;
	int sign = 1;
	if (m_formula[m_position] == MINUS) 
	{
		if (!isNegativeNumericOk())
		{
			throw CalculatorException(CalculatorException::MINUS_UNEXPECTED,
									  m_formula, m_position);
		}
		sign = -1;
		m_position++;
		skipSpace();
		if (isEnd())
			return std::nullopt;
	}

	if (m_formula[m_position] < DIGIT_0 || m_formula[m_position] > DIGIT_9)
		return std::nullopt;

	double number = 0;
	for (; m_position < m_formula.length(); m_position++) 
	{
		int digit = m_formula[m_position] - DIGIT_0;
		if (digit < 0 || digit > 9)
		{
			return sign * number;
		}
		number = 10 * number + digit;
	}
	return sign * number;
}

/////////////////////////////////////////////////////////////////////////////
bool FormulaStream::isNegativeNumericOk()
{
	long signedPosition = (long)m_position;
	for (signedPosition--; signedPosition >= 0; signedPosition--)
	{
		char ch = m_formula[signedPosition];
		if (ch != SPACE)
		{
			return ch == LEFT_BRACKET;
		}
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
void FormulaStream::skipSpace() {
	for (;m_position < m_formula.size(); m_position++)
	{
		char ch = m_formula[m_position];
		if (ch != SPACE)
		{
			return;
		}
	}
}
/////////////////////////////////////////////////////////////////////////////


std::optional<OperationInfo> FormulaStream::readOperation()
{
	skipSpace();
	if (isEnd())
		return std::nullopt;
	char ch = m_formula[m_position];
	size_t pos = m_position;
	switch (ch)
	{
	case PLUS:
		m_position++;
		return OperationInfo{ [](double a, double b) {return a + b; }, SumPriority, pos };
	case MINUS:
		m_position++;
		return OperationInfo{ [](double a, double b) {return a - b; }, SumPriority, pos };
	case MULT:
		m_position++;
		return OperationInfo{ [](double a, double b) {return a * b; }, MultPriority, pos };
	case DIVIDE:
		m_position++;
		return OperationInfo{ [](double a, double b) {return (std::fabs(b) > DBL_EPSILON) ? 
													  std::optional<double>(a / b) 
													  : std::nullopt; }, MultPriority, pos };
	default:
		return std::nullopt;
	}
}

/////////////////////////////////////////////////////////////////////////////
bool FormulaStream::readChar(char ch)
{
	if (m_formula[m_position] == ch)
	{
		m_position++;
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
bool FormulaStream::readOpeningBracket()
{
	skipSpace();
	if (isEnd())
		return false;
	return readChar(LEFT_BRACKET);
}
/////////////////////////////////////////////////////////////////////////////
bool FormulaStream::readClosingBracket()
{
	skipSpace();
	if (isEnd())
		return false;
	return readChar(RIGHT_BRACKET);
}