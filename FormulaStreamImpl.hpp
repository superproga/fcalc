#pragma once
#include "FormulaStream.hpp"

namespace fcalc
{
	class FormulaStream : public IFormulaStream
	{
	public:
		FormulaStream(const std::string& formula);
		const std::string& getFormula() const override { return m_formula; }
		size_t getPosition() const override { return m_position; }

		bool isEnd() override;
		std::optional<double> readNumber() override;
		std::optional<OperationInfo> readOperation() override;
		bool readOpeningBracket() override;
		bool readClosingBracket() override;

	private:
		bool readChar(char ch);
		bool isNegativeNumericOk();
		void skipSpace();
		std::string m_formula;
		size_t      m_position;
	};
}