#pragma once

#include "Calculator.hpp"
#include "FormulaStream.hpp"

namespace fcalc
{
	struct ICalculator
	{
		virtual double calculate(IFormulaStream& formulaStream) = 0;
	};

	class Calculator : public ICalculator
	{
	public:
		Calculator(bool startWithBracket);
		double calculate(IFormulaStream& formulaStream) override;
	private:
		struct Product
		{
			double value = 0;
			std::optional<OperationInfo> nextOperation = std::nullopt;
		};

		double calculateFirstNumber(IFormulaStream& formulaStream);
		void   checkEnd(IFormulaStream& formulaStream);
		Product calculateFirstProduct(IFormulaStream & formulaStream);
		double recalculate(double total, double next, const std::optional<OperationInfo>& op,
						   IFormulaStream& formulaStream);


		bool m_startWithBracket;
	};
}