#include "stdafx.h"
#include <string>
#include "CalculatorException.hpp"
#include "FormulaStream.hpp"

using namespace fcalc;

CalculatorException::CalculatorException(Problem problem,
  									     const std::string& formula,
										  size_t position) :
	std::runtime_error("Calculator error"), 
	m_formula(formula), 
	m_position(position),
	m_problem(problem)
{
}
