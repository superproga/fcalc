#include "stdafx.h"
#include "CppUnitTest.h"
#include "Calculator.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace fcalc;

namespace FCalc
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestResults)
		{
			Assert::AreEqual(2., calculateFormula("1+1"));
			Assert::AreEqual(2., calculateFormula("1 + 1"));
			Assert::AreEqual(2., calculateFormula("(1 + 1)"));

			Assert::AreEqual(6., calculateFormula("2 + 2 * 2"));

			Assert::AreEqual(1., calculateFormula("2 / 2"));

			Assert::AreEqual(8., calculateFormula("2 * (2 + 2)"));

			Assert::AreEqual(6., calculateFormula("1+1+1+1+1+1"));

			Assert::AreEqual(-5., calculateFormula("-5"));
			Assert::AreEqual(0., calculateFormula("-5 + 5"));
			Assert::AreEqual(0., calculateFormula("5 + (-5)"));
		}

		bool checkException(const std::string& formula, size_t pos,
			CalculatorException::Problem problem)
		{
			try
			{
				fcalc::calculateFormula(formula);
			}
			catch (const CalculatorException& ex)
			{
				return ex.getProblem() == problem && ex.getPosition() == pos;
			}
			return false;
		}

		TEST_METHOD(TestExceptions)
		{
			Assert::IsTrue(checkException("(2", 2, CalculatorException::RIGHT_BRACKET_EXPECTED));
			Assert::IsTrue(checkException("", 0, CalculatorException::NUMBER_EXPECTED));
			Assert::IsTrue(checkException("1+", 2, CalculatorException::NUMBER_EXPECTED));
			Assert::IsTrue(checkException("1+-2", 2, CalculatorException::MINUS_UNEXPECTED));

			Assert::IsTrue(checkException("1/0", 1, CalculatorException::DEVIDE_BY_ZERO));
			Assert::IsTrue(checkException("1/(2 + 2 + 2 + 2 - 8)", 1, CalculatorException::DEVIDE_BY_ZERO));
			Assert::AreEqual(1E6, 1 / 0.000001);
		}
	};
}