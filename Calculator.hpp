#pragma once

#include "CalculatorException.hpp"

namespace fcalc
{
	// Calculates formula value.
	// throws CalculatorException if error occures
	double calculateFormula(const std::string formula);
}