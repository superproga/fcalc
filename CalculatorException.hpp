#pragma once
#include<stdexcept>
#include<string>
#include<optional>
#include<functional>

namespace fcalc
{
	class CalculatorException : public std::runtime_error
	{
	public:
		enum Problem
		{
			RIGHT_BRACKET_EXPECTED,
			NUMBER_EXPECTED,
			MINUS_UNEXPECTED,
			DEVIDE_BY_ZERO
		};
		CalculatorException(Problem	problem,
							const std::string& formula,
							size_t position);

		Problem				getProblem() const { return m_problem; }
		const std::string&  getFormula() const { return m_formula; }
		size_t				getPosition() const { return m_position; }
	private:
		std::string m_formula;
		size_t      m_position;
		Problem	    m_problem;
	};
}