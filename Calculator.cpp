#include "stdafx.h"
#include "CalculatorImpl.hpp"
#include "CalculatorException.hpp"
#include "FormulaStreamImpl.hpp"

using namespace fcalc;


Calculator::Calculator(bool startWithBracket):
	m_startWithBracket(startWithBracket)
{

}
/////////////////////////////////////////////////////////////////////////////
double Calculator::calculate(IFormulaStream& formulaStream)
{
	double total = 0;
	std::optional<OperationInfo> prevOperation;
	while (true)
	{
		auto product = calculateFirstProduct(formulaStream);
		total = recalculate(total, product.value, prevOperation, formulaStream);
		if (!product.nextOperation)
			break;
		prevOperation = product.nextOperation;
	}
	checkEnd(formulaStream);
	return total;
}

/////////////////////////////////////////////////////////////////////////////
Calculator::Product Calculator::calculateFirstProduct(IFormulaStream & formulaStream) 
{
	Calculator::Product product;
	double total = calculateFirstNumber(formulaStream);
	while (true)
	{
		std::optional<OperationInfo> nextOperation = formulaStream.readOperation();
		if (nextOperation && nextOperation->priority == MultPriority)
		{
			auto val = calculateFirstNumber(formulaStream);
			total = recalculate(total, val, nextOperation, formulaStream);
		}
		else // !nextOperation || nextOperation->priority == SumPriority
		{
			return { total, nextOperation };
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
void   Calculator::checkEnd(IFormulaStream& formulaStream)
{
	if (m_startWithBracket)
	{
		if (!formulaStream.readClosingBracket())
		{
			throw CalculatorException(CalculatorException::RIGHT_BRACKET_EXPECTED, 
									 formulaStream.getFormula(), 
									 formulaStream.getPosition());
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
double Calculator::calculateFirstNumber(IFormulaStream& formulaStream)
{
	double startingValue = 0;
	if (formulaStream.readOpeningBracket())
	{
		Calculator subCulc(true);
		return subCulc.calculate(formulaStream);
	}

	auto num = formulaStream.readNumber();
	if (num)
	{
		return *num;
	}
	throw CalculatorException(CalculatorException::NUMBER_EXPECTED, 
							  formulaStream.getFormula(),
							  formulaStream.getPosition());
}

/////////////////////////////////////////////////////////////////////////////
double Calculator::recalculate(double total, double next, const std::optional<OperationInfo>& op,
								IFormulaStream& formulaStream) {
	if (!op)
		return next;
	auto res = op->operation(total, next);
	if (res)
		return *res;
	throw CalculatorException(CalculatorException::DEVIDE_BY_ZERO,
							  formulaStream.getFormula(), op->pos);
}

/////////////////////////////////////////////////////////////////////////////

double fcalc::calculateFormula(const std::string formula)
{
	FormulaStream stream(formula);
	Calculator calculator(false);
	return calculator.calculate(stream);
}
